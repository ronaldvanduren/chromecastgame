
Podium = {};
Podium.keydown = function(k) {
    var oEvent = document.createEvent('KeyboardEvent');

    // Chromium Hack
    Object.defineProperty(oEvent, 'keyCode', {
        get : function() {
            return this.keyCodeVal;
        }
    });
    Object.defineProperty(oEvent, 'which', {
        get : function() {
            return this.keyCodeVal;
        }
    });

    if (oEvent.initKeyboardEvent) {
        oEvent.initKeyboardEvent("keydown", true, true, document.defaultView, false, false, false, false, k, k);
    } else {
        oEvent.initKeyEvent("keydown", true, true, document.defaultView, false, false, false, false, k, 0);
    }

    oEvent.keyCodeVal = k;

    if (oEvent.keyCode !== k) {
        alert("keyCode mismatch " + oEvent.keyCode + "(" + oEvent.which + ")");
    }

    document.dispatchEvent(oEvent);
}


//function move(direction, Q){
//    var dom = document.getElementById("quintus");
//    dom.focus();
//    switch(direction){
//        case "left" :
////           document.getElementById("message").innerHTML=direction;
//
//            var down = document.createEvent('KeyboardEvent');
//            delete down.target;
//            delete down.srcElement;
//            delete down.which;
//            delete down.keyCode;
//            delete down.defaultPrevented;
//            delete down.metaKey;
//            Object.defineProperty(down, "metaKey", {"value" : false});
//            Object.defineProperty(down, "defaultPrevented", {"value" : true});
//            Object.defineProperty(down, "keyCode", {"value" : 37});
//            Object.defineProperty(down, "which", {"value" : 37});
//            Object.defineProperty(down, "target", {"value" : dom});
//            Object.defineProperty(down, "srcElement", {"value" : dom});
//
//            down.initKeyboardEvent("keydown", true, true, document.defaultView, "Left", false, false, false, 37, 0);
//
//            Q.el.trigger("keydown", 37);
//            Q.el.trigger("keyup", 37);
//            console.log($(dom));
//
//            var up = document.createEvent('KeyboardEvent');
//
//            delete up.target;
//            delete up.srcElement;
//            delete up.which;
//            delete up.keyCode;
//            delete up.defaultPrevented;
//            delete up.metaKey;
//            Object.defineProperty(up, "metaKey", {"value" : false});
//            Object.defineProperty(up, "defaultPrevented", {"value" : true});
//            Object.defineProperty(up, "keyCode", {"value" : 37});
//            Object.defineProperty(up, "which", {"value" : 37});
//            Object.defineProperty(up, "target", {"value" : dom});
//            Object.defineProperty(up, "srcElement", {"value" : dom});
//
//            up.initKeyboardEvent("keyup", true, true, document.defaultView, "Left", false, false, false, 37, 37);
//
//            console.log(down);
//            console.log(up);
//
//            break;
//        case "right" :
//            $(function () {
//
//                var down = document.createEvent('KeyboardEvent');
//                delete down.target;
//                delete down.srcElement;
//                delete down.which;
//                delete down.keyCode;
//                Object.defineProperty(down, "keyCode", {"value" : 39});
//                Object.defineProperty(down, "which", {"value" : 39});
//                Object.defineProperty(down, "target", {"value" : dom});
//                Object.defineProperty(down, "srcElement", {"value" : dom});
//
//                down.initKeyboardEvent("keydown", true, true, document.defaultView, "Right", false, false, false, 39, 39);
//
//                var up = document.createEvent('KeyboardEvent');
//
//                delete up.target;
//                delete up.srcElement;
//                delete up.which;
//                delete up.keyCode;
//                Object.defineProperty(up, "keyCode", {"value" : 39});
//                Object.defineProperty(up, "which", {"value" : 39});
//                Object.defineProperty(up, "target", {"value" : dom});
//                Object.defineProperty(up, "srcElement", {"value" : dom});
//
//                up.initKeyboardEvent("keyup", true, true, document.defaultView, "Right", false, false, false, 39, 39);
//
//                console.log(down);
//                console.log(up);
//
//            })
//            break;
//        case "up" :
//            document.getElementById("message").innerHTML=direction;
//
//            var down = document.createEvent('KeyboardEvent');
//            delete down.target;
//            delete down.srcElement;
//            delete down.which;
//            delete down.keyCode;
//            Object.defineProperty(down, "keyCode", {"value" : 38});
//            Object.defineProperty(down, "which", {"value" : 38});
//            Object.defineProperty(down, "target", {"value" : dom});
//            Object.defineProperty(down, "srcElement", {"value" : dom});
//
//            down.initKeyboardEvent("keydown", true, true, document.defaultView, "Up", false, false, false, 38, 38);
//
//            var up = document.createEvent('KeyboardEvent');
//
//            delete up.target;
//            delete up.srcElement;
//            delete up.which;
//            delete up.keyCode;
//            Object.defineProperty(up, "keyCode", {"value" : 38});
//            Object.defineProperty(up, "which", {"value" : 38});
//            Object.defineProperty(up, "target", {"value" : dom});
//            Object.defineProperty(up, "srcElement", {"value" : dom});
//
//            up.initKeyboardEvent("keyup", true, true, document.defaultView, "Up", false, false, false, 38, 38);
//
//            console.log(down);
//            console.log(up);
//            break;
//    }
//}


function move(direction, dom){
    //var event = new KeyboardEvent("keydown", {"detail": 39, "which" : 38, "keyCode" : 38, "bubbles" : true, "cancelable" : true, "view" : window});
    //var event = new CustomEvent("keydown", {"detail" : 37});
    //var event2 = new CustomEvent("keyup", {"detail" : 37});
    //var event2 = new KeyboardEvent("keyup", {"detail": 39, "which" : 38, "keyCode" : 38, "bubbles" : true, "cancelable" : true, "view" : window});
    var tempDom = document.getElementById("quintus");
    tempDom.focus();
    //console.log(tempDom);
    switch (direction){
        case "left" :
            var event = new CustomEvent("keydown", {"detail" : 37});
            tempDom.dispatchEvent(event);
            break;
        case "right":
            var event = new CustomEvent("keydown", {"detail" : 39});
            tempDom.dispatchEvent(event);
            break;
        case "up" :
            var event = new CustomEvent("keydown", {"detail" : 38});
            tempDom.dispatchEvent(event);
            break;
        case "replay" :
            var button = document.getElementsByName("button");
            var event = new MouseEvent("click");
            button.dispatchEvent(event);
            break;
    }
}

function test(dom){

    var data = {"data" : 37};
    $(dom.el).trigger("keyup", 37);

    if($(dom.el).keydown(37)){
        console.log($(dom.el));
    }
}