down.type = "keydown";
delete down.which;
Object.defineProperty(down, "which", {"value" : 39});

delete down.keyIdentifier;
Object.defineProperty(down, "keyIdentifier", {"value" : "Right"});

delete down.keyCode;
Object.defineProperty(down, "key", {"value": 39});
down.target = document.getElementById("quintus");

var dom = document.getElementById("quintus");
delete down.srcElement;
Object.defineProperty(down, "srcElement", {"value" : dom});

delete down.target;
Object.defineProperty(down, "target", {"value" : dom});

//                var window = this.contentWindow;
//
//                delete down.view;
//                Object.defineProperty(down, "view", {"value" : window});

delete down.bubbles;
Object.defineProperty(down, "bubbles", {"value": true});

delete down.cancelable;
Object.defineProperty(down, "cancelable", {"value": true});

delete down.defaultPrevented;
Object.defineProperty(down, "defaultPrevented", {"value": true});

delete down.returnValue;
Object.defineProperty(down, "returnValue", {"value": false});


//$("#quintus").trigger(down);
console.log(down);

var up =  document.createEvent("keyboardevent");
up.type = "keyup";
delete up.which;
Object.defineProperty(up, "which", {"value" : 39});

delete up.keyIdentifier;
Object.defineProperty(up, "keyIdentifier", {"value" : "Right"});

delete up.keyCode;
Object.defineProperty(up, "key", {"value": 39});

var dom = document.getElementById("quintus");
delete up.srcElement;
Object.defineProperty(up, "srcElement", {"value" : dom});

delete up.target;
Object.defineProperty(up, "target", {"value" : dom});

var window = this.contentWindow;

//                delete up.view;
//                Object.defineProperty(up, "view", {"value" : window});

delete up.bubbles;
Object.defineProperty(up, "bubbles", {"value": true});

delete up.cancelable;
Object.defineProperty(up, "cancelable", {"value": true});

delete up.defaultPrevented;
Object.defineProperty(up, "defaultPrevented", {"value": true});

delete up.returnValue;
Object.defineProperty(up, "returnValue", {"value": false});
