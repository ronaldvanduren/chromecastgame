/**
 * Created by Ronald on 15-9-14.
 */

var xPosition = 100;
var yPosition = 100;
var image = new Image();
image.src = "/ball.png";
canvas = document.getElementById("gameView");
window.ctx = canvas.getContext("2d");
var imageWidth = (image.width = 30);
var imageHeight = (image.height = 30);
var steps = 10;
var currentAmountOfBullets = 0;
var worker;
var enemy;
var leftOn = true;
var rightOn = true;
function getYPosition(){
    return this.yPosition;
}

function getXPosition(){
    return this.xPosition;
}

function getCanvas(){
    return this.canvas;
}

function getWorker(){
    return this.worker;
}


/**
 * When this function is called with a correct direction(Up, Down, Left or Right)
 * the image is repainted on the new coordinates
 * @param direction (String)
 */
function move(direction){
    switch (direction){
        case "up" :
            shoot();
            break;
        case "left" :
            rightOn = false;
            //while(leftOn == true){
            xPosition = (xPosition - steps);
            //repaint(image, xPosition, yPosition, "left");
           // }
            break;
        case "right" :
            leftOn = false;
           // while(rightOn == true){
            xPosition = (xPosition + steps);
            //repaint(image, xPosition, yPosition, "right");
           // }
            break;
    }
}

function shoot(){
   if(currentAmountOfBullets < 1){

       worker = new Worker("bullet.js");

       worker.onerror = function(message){
           console.error(message);
       };

       worker.addEventListener('message', function(e){
           if(e.data == "decrementBullets"){
               currentAmountOfBullets--;
           }
           paintBullet((e.data.xPosition + (imageWidth / 2)), (e.data.yPosition + (imageHeight / 2)));
       }, false);

       worker.postMessage({"xPosition" : xPosition, "yPosition" : yPosition});

       currentAmountOfBullets++;
   }
}

function checkCollision(){

}

function checkIfOutOfBounds(){

}

function repaint(image, xPosition, yPosition, direction){
    switch(direction){
        case true :
            ctx.drawImage(image, xPosition, yPosition, imageWidth, imageHeight);
            ctx.clearRect((xPosition + imageWidth ), yPosition, 10, 30);
            break;
        case false :
            ctx.drawImage(image, xPosition, yPosition, imageWidth, imageHeight);
            ctx.clearRect((xPosition - steps), yPosition, 10, 30);
            break;
        default :
            ctx.drawImage(image, xPosition, yPosition, imageWidth, imageHeight);
            break;
    }
}

function initialize(){
    repaint(image, xPosition, yPosition, "init");
   // enemy = new Worker("enemy.js");

//    enemy.addEventListener("message", function(e){
//        enemy.postMessage();
//    }, false);

    while(true){}
}

function removeWorker(){
    this.worker.terminate();
    this.worker = undefined;
}

function paintBullet(x, y){
    //ctx.rect(x, y, 5, 10);
    console.log("X=" + x + "Y=" + y);
    ctx.fillStyle = "yellow";
    ctx.fillRect(x, y, 5, 5);
    ctx.clearRect(x, (y + 1), 5, 5);
    console.log(y);
}

