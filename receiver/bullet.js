/**
 * Created by Ronald on 16-9-14.
 */

var x;
var y;
var velocity = 2;
var bHeight;
var bWidth;
var canMove;
var parsedCanvas;
//var image = new Image();
//image.src = "pathToImage";
//tempCtx = ctx;

function Bullet(xPosition, yPosition){
    velocity = 4;
    bHeight = 10;
    bWidth = 5;
    canMove = true;
    x = xPosition;
    y = yPosition;
    fire();
}

function fire(){
    var i = setInterval(function(){
        if(y < 0){
            clearInterval(i);
            self.postMessage("decrementBullets");
            self.close();
        }
        y = (y - velocity);
        self.postMessage({"xPosition" : x, "yPosition" : y});
    }, 50);
}

self.addEventListener('message', function(e){
   //self.postMessage(e.data);
   x = e.data.xPosition;
   y = e.data.yPosition;
    self.postMessage("fire");
    fire();
}, false);