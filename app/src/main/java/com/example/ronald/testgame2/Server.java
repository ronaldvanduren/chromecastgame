package com.example.ronald.testgame2;

import android.os.Environment;
import android.provider.Settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import fi.iki.elonen.IWebSocketFactory;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.WebSocket;
import fi.iki.elonen.WebSocketFrame;
import fi.iki.elonen.WebSocketResponseHandler;

/**
 * Created by Ronald on 11-9-2014.
 */
public class Server extends NanoHTTPD {

    WebSocketResponseHandler responseHandler;

    IWebSocketFactory webSocketFactory = new IWebSocketFactory() {

        @Override
        public WebSocket openWebSocket(IHTTPSession handshake) {
            return new Ws(handshake);
        }
    };


    public Server(int port) {
        super(port);
        responseHandler = new WebSocketResponseHandler(webSocketFactory);
    }
    @Override
    public Response serve(IHTTPSession session) {
        NanoHTTPD.Response ws = responseHandler.serve(session);
        if(ws == null){
            String uri = session.getUri();
            if(uri.equals("/")) {
                try {
                    //System.out.println(Environment.DIRECTORY_DOCUMENTS.toString() + "/receiver.html");
                    File file = new File(Environment.getExternalStorageDirectory() + "/Documents/game/receiver.html");
                    return new NanoHTTPD.Response(NanoHTTPD.Response.Status.OK, "text/html", new FileInputStream(file));
                } catch (FileNotFoundException e) {
                    System.out.println("File not found! " + e.getMessage());
                    //System.out.println("Current location: " + location);
                    return new NanoHTTPD.Response("hello, i am running....");
                }
            }
            else if(uri.contains(".js")){
                try {
                    String filename = uri.substring(1);
                    File javascript = new File(Environment.getExternalStorageDirectory() + "/Documents/game/" + filename);
                    return new Response(Response.Status.OK, "application/javascript", new FileInputStream(javascript));
                }catch(Exception e){
                    System.out.println("Error reading file: " + e);
                }
            }else if(uri.contains(".png")){
                try{
                    String filename = uri.substring(1);
                    File image = new File(Environment.getExternalStorageDirectory() + "/Documents/game/" + filename);
                    return new Response(Response.Status.OK, "image/png", new FileInputStream(image));
                }catch(Exception e){
                    System.out.println("Error reading file: " + e);
                }
            }
        }
        return ws;
    }

    class Ws extends WebSocket {

        public Ws(IHTTPSession handshakeRequest) {
            super(handshakeRequest);
            System.out.println("user connected......");
        }

        @Override
        protected void onPong(WebSocketFrame pongFrame) {
            System.out.println("user pong.......");
        }

        @Override
        protected void onMessage(WebSocketFrame messageFrame) {
            System.out.println("user message : " + messageFrame.getTextPayload());
            try {
                send("i am server and say hello......");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onClose(WebSocketFrame.CloseCode code, String reason,
                               boolean initiatedByRemote) {
            System.out.println("user disconnected.....");
        }

        @Override
        protected void onException(IOException e) {
            System.out.println(e.getMessage());
        }

    }

}
